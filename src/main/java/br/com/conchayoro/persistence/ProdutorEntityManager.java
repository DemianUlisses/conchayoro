package br.com.conchayoro.persistence;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

@RequestScoped
public class ProdutorEntityManager {

	@PersistenceUnit
	EntityManagerFactory factory;

	@Produces
	@RequestScoped
	public EntityManager newEntityManager() {
		return factory.createEntityManager();
	}

	public void closeEntityManager(@Disposes EntityManager em) {
		em.close();
	}
}
